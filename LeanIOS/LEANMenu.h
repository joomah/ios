//
//  LEANMenu.h
//  LeanIOS
//
//  Created by Weiyin He on 2/8/14.
// Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LEANMenuItem.h"

@interface LEANMenu : NSObject

@property BOOL loggedIn;
@property NSString* userName;
@property NSString* userBio;
@property NSURL* avatarUrl;
@property NSArray* menuItems;

+(LEANMenu*) menuWithContentsOfUrl:(NSURL*)url;

@end
