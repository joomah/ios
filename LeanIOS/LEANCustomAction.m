//
//  LEANCustomAction.m
//  GoNativeIOS
//
//  Created by Weiyin He on 4/17/14.
// Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import "LEANCustomAction.h"

@implementation LEANCustomAction
+ (NSArray*)actionsForUrl:(NSURL*)url
{
    if (url == nil) {
        return nil;
    }
    // TODO: read from config. Currently hardcoded for Insight Squared
    
    NSString *path = [url path];
    if ([path hasSuffix:@"reports/bookings2"] || [path hasSuffix:@"reports/sales_trend"]) {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        
        LEANCustomAction *action;
        
        action = [[LEANCustomAction alloc] init];
        action.name = @"Chart";
        action.javascript = @"$('.chart-container .highcharts-container').show(); $('.chart-container #istable').hide();";
        [array addObject:action];
        
        action = [[LEANCustomAction alloc] init];
        action.name = @"Table";
        action.javascript = @"$('.chart-container .highcharts-container').hide(); $('.chart-container #istable').show();";
        [array addObject:action];
        
        return array;
    }
    
    return nil;
}
@end
