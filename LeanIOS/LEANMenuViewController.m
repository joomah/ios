//
//  LEANMenuViewController.m
//  GoNativeIOS
//
//  Created by Weiyin He on 2/7/14.
//  Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import "LEANMenuViewController.h"
#import "LEANNavigationController.h"
#import "LEANWebViewController.h"
#import "LEANRootViewController.h"
#import "LEANAppConfig.h"
#import "LEANLoginManager.h"
#import "FontAwesome/NSString+FontAwesome.h"
#import "FontAwesome/UIFont+FontAwesome.h"
#import "LEANUrlInspector.h"
#import "LEANSettingsController.h"

@interface LEANMenuViewController () <UIAlertViewDelegate>

@property id menuItems;
@property NSMutableArray *groupExpanded;

@property LEANWebViewController *wvc;

@property UIImage *collapsedIndicator;
@property UIImage *expandedIndicator;
@property UIButton *settingsButton;
@property UIPopoverController *settingsPopover;

@property BOOL groupsHaveIcons;
@property BOOL childrenHaveIcons;

@end

@implementation LEANMenuViewController



- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.sectionHeaderHeight = 0;
    self.tableView.sectionFooterHeight = 0;
    
    NSArray *arr = [[NSBundle mainBundle] loadNibNamed:@"HeaderView" owner:nil options:nil];

    UIView *headerView = arr[0];
    UIButton *headerButton = headerView.subviews[0];
    [headerButton addTarget:self action:@selector(picturePressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.settingsButton = (UIButton*)[headerView viewWithTag:2];
    [self.settingsButton addTarget:self action:@selector(settingsPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableView.tableHeaderView = headerView;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    
    [self updateMenu: NO];

    if ([[LEANAppConfig sharedAppConfig][@"checkUserAuth"] boolValue]) {
        // subscribe to login notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveLoginNotification:) name:kLEANLoginManagerNotificationName object:nil];
        [[LEANLoginManager sharedManager] checkIfNotAlreadyChecking];
    }

    
    // pre-load images. Color them too.
    self.collapsedIndicator = [[UIImage imageNamed:@"chevronDown"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.expandedIndicator = [[UIImage imageNamed:@"chevronUp"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // webviewcontroller reference
    LEANNavigationController* nav = (LEANNavigationController*)self.frostedViewController.contentViewController;
    self.wvc = (LEANWebViewController*)nav.viewControllers[0];
    
}

- (void)didReceiveLoginNotification:(NSNotification*)notification
{
    LEANLoginManager *loginManager = [notification object];
    [self updateMenu:loginManager.loggedIn];
}

- (void)updateMenu:(BOOL)loggedIn
{
    NSString *path;
    if (loggedIn)
        path = [[NSBundle mainBundle] pathForResource:@"menu_loggedin" ofType:@"json"];
    else
        path = [[NSBundle mainBundle] pathForResource:@"menu_default" ofType:@"json"];
    
    // parse JSON
    NSInputStream *inputStream = [NSInputStream inputStreamWithFileAtPath:path];
    [inputStream open];
    self.menuItems = [NSJSONSerialization JSONObjectWithStream:inputStream options:0 error:nil];
    [inputStream close];
    
    // see if any menu items have icons. Used for layout indentation.
    self.groupsHaveIcons = NO;
    self.childrenHaveIcons = NO;
    for (id item in self.menuItems) {
        if (item[@"icon"] && item[@"icon"] != [NSNull null]) {
            self.groupsHaveIcons = YES;
        }
        
        if ([item[@"isGrouping"] boolValue]) {
            for (id sublink in item[@"subLinks"]) {
                if (sublink[@"icon"] && sublink[@"icon"] != [NSNull null]) {
                    self.childrenHaveIcons = YES;
                    break;
                }
            }
        }
    }
    
    // groups are initially collapsed
    self.groupExpanded = [[NSMutableArray alloc] initWithCapacity:[self.menuItems count]];
    for (int i = 0; i < [self.menuItems count]; i++) {
        self.groupExpanded[i] = [NSNumber numberWithBool:NO];
    }
    
    [self.tableView reloadData];
}

- (void)logOut
{
    [self.wvc logout];
    [self updateMenu:NO];
}

- (void)showSettings:(BOOL)showSettings
{
    [self.settingsButton setUserInteractionEnabled:showSettings];
    [UIView animateWithDuration:0.3 animations:^(void){
        self.settingsButton.alpha = showSettings ? 1.0 : 0.0;
    }];
}

- (IBAction)picturePressed:(id)sender {
    [self.wvc loadUrl:[LEANAppConfig sharedAppConfig].initialURL];
    [self.frostedViewController hideMenuViewController];
}

- (IBAction)settingsPressed:(id)sender
{
    LEANSettingsController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsController"];
    controller.profilePicker = self.wvc.profilePicker;
    controller.wvc = self.wvc;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.settingsPopover = [[UIPopoverController alloc] initWithContentViewController:controller];
        controller.popover = self.settingsPopover;
        [self.settingsPopover presentPopoverFromRect:[self.settingsButton bounds] inView:self.settingsButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    } else {
        [self.frostedViewController hideMenuViewController];
        [self.wvc.navigationController pushViewController:controller animated:YES];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.menuItems count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.groupExpanded[section] boolValue]) {
        return 1 + [self.menuItems[section][@"subLinks"] count];
    }
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellNib;
    UITableViewCell *cell;
    UIImageView *imageView;
    UILabel *label;
    if (indexPath.row == 0) {
        if (self.groupsHaveIcons) cellNib = @"MenuGroupIcon";
        else cellNib = @"MenuGroupNoIcon";
    } else {
        if (self.groupsHaveIcons || self.childrenHaveIcons) cellNib = @"MenuChildIcon";
        else cellNib = @"MenuChildNoIcon";
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellNib];
    if (nil == cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:cellNib owner:nil options:nil][0];
        cell.backgroundColor = [UIColor clearColor]; // shouldn't need this, but background is white on ipad if I don't do this.
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, 240, 1)];
        separatorLineView.backgroundColor = [UIColor colorWithHue:0 saturation:0 brightness:.7 alpha:.5];
        separatorLineView.tag = 99;
        [cell.contentView addSubview:separatorLineView];
    }
    
    label = (UILabel*)[cell.contentView viewWithTag:1];
    imageView = (UIImageView*)[cell.contentView viewWithTag:2];
    
    // get data
    id menuItem;
    if (indexPath.row == 0) {
        menuItem = self.menuItems[indexPath.section];
    } else {
        menuItem = self.menuItems[indexPath.section][@"subLinks"][indexPath.row - 1];
    }
    
    // text
    label.text = [menuItem[@"name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // expand/collapse indicator
    if ([menuItem[@"isGrouping"] boolValue]) {
        if ([self.groupExpanded[indexPath.section] boolValue]) {
            cell.accessoryView = [[UIImageView alloc] initWithImage:self.expandedIndicator];
        } else {
            cell.accessoryView = [[UIImageView alloc] initWithImage:self.collapsedIndicator];
        }
    } else
        cell.accessoryView = nil;
    
    // icon
    UILabel *icon;
    if (menuItem[@"icon"] && menuItem[@"icon"] != [NSNull null] && [menuItem[@"icon"] hasPrefix:@"fa-"]) {
        // add icon to imageView
        icon = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, imageView.bounds.size.width, imageView.bounds.size.height)];
        icon.textAlignment = NSTextAlignmentCenter;
        icon.font = [UIFont fontAwesomeFontOfSize:label.font.pointSize];
        icon.text = [NSString fontAwesomeIconStringForIconIdentifier:menuItem[@"icon"]];
        [imageView addSubview:icon];
    } else {
        imageView.image = nil;
        [[imageView subviews]
         makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    // configure text color
    if ([[LEANAppConfig sharedAppConfig][@"checkCustomStyling"] boolValue]) {
        label.textColor = [LEANAppConfig sharedAppConfig].tintColor;
        icon.textColor = [LEANAppConfig sharedAppConfig].tintColor;
        cell.tintColor = [LEANAppConfig sharedAppConfig].tintColor;
    }
    
    // hide separator line from first cell
    if (indexPath.section == 0 && indexPath.row == 0) {
        [cell.contentView viewWithTag:99].hidden = YES;
    } else {
        [cell.contentView viewWithTag:99].hidden = NO;
    }
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *url = nil;
    
    // if is first row, then check if it is grouped.
    if (indexPath.row == 0) {
        if ([self.menuItems[indexPath.section][@"isGrouping"] boolValue]) {
            self.groupExpanded[indexPath.section] = [NSNumber numberWithBool:![self.groupExpanded[indexPath.section] boolValue]];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else {
            url = self.menuItems[indexPath.section][@"url"];
        }
    } else {
        // regular child
        url = self.menuItems[indexPath.section][@"subLinks"][indexPath.row - 1][@"url"];
    }
    
    if (url != nil) {
        // check for GONATIVE_USERID string.
        url = [url stringByReplacingOccurrencesOfString:@"GONATIVE_USERID" withString:[LEANUrlInspector sharedInspector].userId];
        
        if ([url hasPrefix:@"javascript:"]) {
            NSString *js = [url substringFromIndex: [@"javascript:" length]];
            [self.wvc runJavascript:js];
        } else {
            [self.wvc loadUrl:[NSURL URLWithString:url]];
        }
        [self.frostedViewController hideMenuViewController];
    }
    
    /*
    LEANMenuItem* menuItem = self.menu.menuItems[indexPath.row];
    
    if ([menuItem.title isEqualToString:@"Log Out"]) {
        [[[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Log Out", nil] show];
    }
    else {
        [self.wvc loadUrl: menuItem.url];
    }
    
    [self.frostedViewController hideMenuViewController]; */
}

#pragma mark - Alert view delegate
- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self logOut];
    }
}

@end


