//
//  LEANMenuItem.m
//  GoNativeIOS
//
//  Created by Weiyin He on 2/8/14.
//  Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import "LEANMenuItem.h"

@implementation LEANMenuItem


- (LEANMenuItem*) initWithTitle:(NSString*)title
{
    return [self initWithTitle:title url:nil];
}

- (LEANMenuItem*) initWithTitle:(NSString*)title url:(NSURL*) url
{
    self = [super init];
    self.title = title;
    self.url = url;
    
    return self;
}

@end
