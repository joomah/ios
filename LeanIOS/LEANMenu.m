//
//  LEANMenu.m
//  LeanIOS
//
//  Created by Weiyin He on 2/8/14.
// Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import "LEANMenu.h"
#import "LEANAppDelegate.h"
#import "LEANAppConfig.h"

@interface LEANMenu () <NSXMLParserDelegate>


typedef enum ParseStatusType : NSInteger ParseStatusType;
enum ParseStatusType : NSInteger {
    ParseStatusNone, ParseStatusUser, ParseStatusMenu
};


@property NSXMLParser* xmlParser;
@property ParseStatusType parseStatus;
@property NSMutableString* tempParseString;
@property NSMutableArray* tempParseArray;

@end

@implementation LEANMenu




+(LEANMenu*) menuWithContentsOfUrl:(NSURL*)url
{
    LEANMenu* menu = [[LEANMenu alloc] init];
    menu.xmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [menu.xmlParser setDelegate:menu];
    menu.parseStatus = ParseStatusNone;
    
    if (![menu.xmlParser parse]){
        NSLog(@"Failed to parse the XML");
    }
    
    return menu;
}

- (void) parserDidStartDocument:(NSXMLParser *)parser
{
    
}

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"user"])
    {
        self.loggedIn = YES;
        self.parseStatus = ParseStatusUser;
    }
    else if (self.parseStatus == ParseStatusUser &&
             ([elementName isEqualToString:@"name"] ||
              [elementName isEqualToString:@"Avatar"] ||
              [elementName isEqualToString:@"Bio"]))
    {
        self.tempParseString = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"menu"])
    {
        self.tempParseArray = [[NSMutableArray alloc] init];
        self.parseStatus = ParseStatusMenu;
    }
    else if ([elementName isEqualToString:@"link"])
    {
        LEANAppConfig * appConfig = [LEANAppConfig sharedAppConfig];
        NSString* prepend = appConfig[@"menuLinkPrefix"];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", prepend, attributeDict[@"URL"]]];
        
        [self.tempParseArray addObject:[[LEANMenuItem alloc]
                                         initWithTitle:attributeDict[@"name"] url:url]];
    }
    else if([elementName isEqualToString:@"notloggedin"])
        self.loggedIn = NO;
    
}

- (void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [self.tempParseString appendString:string];
}

- (void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    if (self.parseStatus == ParseStatusUser)
    {
        if ([elementName isEqualToString:@"user"])
            self.parseStatus = ParseStatusNone;
        else if ([elementName isEqualToString:@"name"])
            self.userName = self.tempParseString;
        else if ([elementName isEqualToString:@"Avatar"])
            self.avatarUrl = [NSURL URLWithString:self.tempParseString];
        else if ([elementName isEqualToString:@"Bio"])
            self.userBio = self.tempParseString;
    }
    else if(self.parseStatus == ParseStatusMenu && [elementName isEqualToString:@"menu"])
    {
        self.menuItems = self.tempParseArray;
        self.parseStatus = ParseStatusNone;
    }
    
    self.tempParseString = nil;
    
}

@end
