//
//  LEANMenuItem.h
//  GoNativeIOS
//
//  Created by Weiyin He on 2/8/14.
//  Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LEANMenuItem : NSObject

@property NSString* title;
@property NSURL* url;

- (LEANMenuItem*) initWithTitle:(NSString*)title;
- (LEANMenuItem*) initWithTitle:(NSString*)title url:(NSURL*) url;


@end
