//
//  LEANUrlInspector.m
//  GoNativeIOS
//
//  Created by Weiyin He on 4/22/14.
// Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import "LEANUrlInspector.h"

@interface LEANUrlInspector ()
@property NSRegularExpression *userIdRegex;
@end

@implementation LEANUrlInspector

+ (LEANUrlInspector *)sharedInspector
{
    static LEANUrlInspector *sharedInspector;
    
    @synchronized(self)
    {
        if (!sharedInspector){
            sharedInspector = [[LEANUrlInspector alloc] init];
            
            sharedInspector.userId = @"";
            sharedInspector.userIdRegex = [NSRegularExpression regularExpressionWithPattern:@"https://mixpanel.com/report/(\\d+)/" options:0 error:nil];
        }
        
        return sharedInspector;
    }
}

- (void)inspectUrl:(NSURL *)url
{
    NSString *urlString = [url absoluteString];
    
    NSTextCheckingResult *result = [self.userIdRegex firstMatchInString:urlString options:0 range:NSMakeRange(0, [urlString length])];
    
    // first range is the entire regex. Second range is the (selection)
    if ([result numberOfRanges] >= 2) {
        self.userId = [urlString substringWithRange:[result rangeAtIndex:1]];
    }
}

@end
