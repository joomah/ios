//
//  LEANAppConfig.m
//  LeanIOS
//
//  Created by Weiyin He on 2/10/14.
// Copyright (c) 2014 GoNative.io LLC. All rights reserved.
//

#import "LEANAppConfig.h"
#import "LEANUtilities.h"
#import <CommonCrypto/CommonCrypto.h>

@interface LEANAppConfig ()
@property id json;
@end

@implementation LEANAppConfig

+ (LEANAppConfig *)sharedAppConfig
{
    static LEANAppConfig *sharedAppConfig;
    
    @synchronized(self)
    {
        if (!sharedAppConfig){
            sharedAppConfig = [[LEANAppConfig alloc] init];
            
            NSString *phantom = @"7V70kMJooeFZLi5HquP/vVu7AJNi5DKxhAlwc8iSx44="; // AES key
            NSString *sparky = @"/f1VYmJ5tmUvdFOSStjixA=="; // iv
            
            if ([phantom length] > 0) {
                NSData *key = [[NSData alloc] initWithBase64EncodedString:phantom options:0];
                NSData *iv = [[NSData alloc] initWithBase64EncodedString:sparky options:0];
                
                NSString *path = [[NSBundle mainBundle] pathForResource:@"appConfig" ofType:@"plist.enc"];
                NSData *encrypted = [NSData dataWithContentsOfFile:path];
                
                // Going to call a C function. The decrypted length is at most the encrypted
                // length plus one block.
                size_t bufferSize = [encrypted length] + kCCBlockSizeAES128;
                void *buffer = malloc(bufferSize);
                size_t numBytesDecrypted = 0;
                
                CCCryptorStatus status = CCCrypt(kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding, [key bytes], [key length], [iv bytes], [encrypted bytes], [encrypted length], buffer, bufferSize, &numBytesDecrypted);
                
                if (status == kCCSuccess){
                    NSData *decrypted = [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted freeWhenDone:YES]; // takes ownership of buffer
                    
                    NSPropertyListFormat format = NSPropertyListXMLFormat_v1_0;
                    NSError *err = [[NSError alloc] init];
                    
                    sharedAppConfig.dict = [NSPropertyListSerialization propertyListWithData:decrypted options:NSPropertyListImmutable format:&format error:&err];
                }
                else {
                    free(buffer);
                }
            }
            else {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"appConfig" ofType:@"plist"];
                sharedAppConfig.dict = [[NSDictionary alloc] initWithContentsOfFile:path];
            }
            
            sharedAppConfig.tintColor = [LEANUtilities colorFromHexString:sharedAppConfig[@"iosTintColor"]];
            
            sharedAppConfig.titleTextColor = [LEANUtilities colorFromHexString:sharedAppConfig[@"iosTitleColor"]];
            sharedAppConfig.initialURL = [NSURL URLWithString:sharedAppConfig[@"initialURL"]];
            sharedAppConfig.initialHost = [sharedAppConfig.initialURL host];
            if ([sharedAppConfig.initialHost hasPrefix:@"www."]) {
                sharedAppConfig.initialHost = [sharedAppConfig.initialHost stringByReplacingCharactersInRange:NSMakeRange(0, [@"www." length]) withString:@""];
            }
            sharedAppConfig.loginDetectionURL = [NSURL URLWithString:sharedAppConfig[@"loginDetectionURL"]];
            sharedAppConfig.loginDetectionURLnotloggedin = [NSURL URLWithString:sharedAppConfig[@"loginDetectionURLnotloggedin"]];
            sharedAppConfig.loginURL = [NSURL URLWithString:sharedAppConfig[@"loginURL"]];
            sharedAppConfig.loginURLfail = [NSURL URLWithString:sharedAppConfig[@"loginURLfail"]];
            sharedAppConfig.forgotPasswordURL = [NSURL URLWithString:sharedAppConfig[@"forgotPasswordURL"]];
            sharedAppConfig.forgotPasswordURLfail = [NSURL URLWithString:sharedAppConfig[@"forgotPasswordURLfail"]];
            sharedAppConfig.signupURL = [NSURL URLWithString:sharedAppConfig[@"signupURL"]];
            sharedAppConfig.signupURLfail = [NSURL URLWithString:sharedAppConfig[@"signupURLfail"]];
            sharedAppConfig.loginIsFirstPage = [sharedAppConfig[@"loginIsFirstPage"] boolValue];
            sharedAppConfig.showShareButton = [sharedAppConfig[@"showShareButton"] boolValue];
            sharedAppConfig.enableChromecast = [sharedAppConfig[@"enableChromecast"] boolValue];
            
            if (sharedAppConfig[@"forceLandscapeRegex"]) {
                sharedAppConfig.forceLandscapeMatch = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", sharedAppConfig[@"forceLandscapeRegex"]];
            }
            
            if (sharedAppConfig[@"allowZoom"]) {
                sharedAppConfig.allowZoom = [sharedAppConfig[@"allowZoom"] boolValue];
            }
            else
                sharedAppConfig.allowZoom = YES;
            
            
            // read json
            NSString *path = [[NSBundle mainBundle] pathForResource:@"appConfig" ofType:@"json"];
            NSInputStream *inputStream = [NSInputStream inputStreamWithFileAtPath:path];
            [inputStream open];
            sharedAppConfig.json = [NSJSONSerialization JSONObjectWithStream:inputStream options:0 error:nil];
            [inputStream close];
            
            if ([sharedAppConfig hasKey:@"redirects"]) {
                NSUInteger len = [sharedAppConfig[@"redirects"] count];
                sharedAppConfig.redirects = [[NSMutableDictionary alloc] initWithCapacity:len];
                for (id redirect in sharedAppConfig[@"redirects"]) {
                    [sharedAppConfig.redirects setValue:redirect[@"to"] forKey:redirect[@"from"]];
                }
            }
            
            if ([sharedAppConfig hasKey:@"showToolbar"])
                sharedAppConfig.showToolbar = [sharedAppConfig[@"showToolbar"] boolValue];
            else sharedAppConfig.showToolbar = YES;
            
            if ([sharedAppConfig hasKey:@"showNavigationBar"])
                sharedAppConfig.showNavigationBar = [sharedAppConfig[@"showNavigationBar"] boolValue];
            else sharedAppConfig.showNavigationBar = YES;
            
            if ([sharedAppConfig hasKey:@"pushNotifications"])
                sharedAppConfig.pushNotifications = [sharedAppConfig[@"pushNotifications"] boolValue];
            else sharedAppConfig.pushNotifications = NO;
            
            if ([sharedAppConfig hasKey:@"navStructure"]) {
                id urlLevels = sharedAppConfig[@"navStructure"][@"urlLevels"];
                sharedAppConfig.navStructureLevels = [[NSMutableArray alloc] initWithCapacity:[urlLevels count]];
                
                for (id entry in urlLevels) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", entry[@"regex"]];
                    NSNumber *level = entry[@"level"];
                    
                    [sharedAppConfig.navStructureLevels addObject:@{@"predicate": predicate, @"level": level}];
                }
                
                id titles = sharedAppConfig[@"navStructure"][@"titles"];
                sharedAppConfig.navTitles = [[NSMutableArray alloc] initWithCapacity:[titles count]];
                
                for (id entry in titles) {
                    NSMutableDictionary *toAdd = [[NSMutableDictionary alloc] init];
                    
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", entry[@"regex"]];
                    [toAdd setObject:predicate forKey:@"predicate"];
                    
                    
                    if (entry[@"title"]) {
                        [toAdd setObject:entry[@"title"] forKey:@"title"];
                    }

                    if (entry[@"urlRegex"]) {
                        [toAdd setObject:[NSRegularExpression regularExpressionWithPattern:entry[@"urlRegex"] options:0 error:nil] forKey:@"urlRegex"];
                    }
                    
                    if (entry[@"urlChompWords"]) {
                        [toAdd setObject:entry[@"urlChompWords"] forKey:@"urlChompWords"];
                    }
                    
                    [sharedAppConfig.navTitles addObject:toAdd];
                }
            }
            
            if ([sharedAppConfig hasKey:@"interactiveDelay"]) {
                sharedAppConfig.interactiveDelay = sharedAppConfig[@"interactiveDelay"];
            }
            
            if ([sharedAppConfig hasKey:@"interceptForms"]) {
                sharedAppConfig.interceptForms = sharedAppConfig[@"interceptForms"];
            }
            
            if ([sharedAppConfig hasKey:@"regexInternalExternal"]) {
                id temp = sharedAppConfig[@"regexInternalExternal"];
                
                NSUInteger num = [temp count];
                sharedAppConfig.regexInternalEternal = [[NSMutableArray alloc] initWithCapacity:num];
                sharedAppConfig.regexIsInternal = [[NSMutableArray alloc] initWithCapacity:num];
                for (id entry in temp) {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", entry[@"regex"]];
                    NSNumber *internal = entry[@"internal"];
                    if (predicate) {
                        [sharedAppConfig.regexInternalEternal addObject:predicate];
                        [sharedAppConfig.regexIsInternal addObject:internal];
                    }
                }
            }
        }
        
        return sharedAppConfig;
    }
}

- (BOOL)hasKey:(id)key
{
    return (self.json[key] && self.json[key] != [NSNull null]) || self.dict[key];
}

- (id)objectForKey:(id)aKey;
{
    // first check json
    if (self.json[aKey]) {
        return self.json[aKey];
    } else
        return [self.dict objectForKey:aKey];
}

- (id)objectForKeyedSubscript:(id)key
{
    return [self objectForKey:key];
}

@end
